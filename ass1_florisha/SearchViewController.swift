//
//  UIViewController.swift
//  ass1_florisha
//
//  Created by Jinal on 2018-06-13.
//  Copyright © 2018 Jinal. All rights reserved.
//

import UIKit


class SearchViewController: UIViewController {
    var searchParam: String?
    
    // MARK: - View Controller LifeCycle
    override func viewDidLoad() {
        super.viewDidLoad()
        searchParameterTextField.delegate = self
        // Do any additional setup after loading the view.
    }
    
    @IBOutlet weak var searchParameterTextField: UITextField!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //  MARK: - Navigation
    @IBAction func searchButtonAction(_ sender: Any) {
        print("button clicked")
        
        searchParameterTextField.resignFirstResponder()
        
        if let inputValue = searchParam, inputValue.count > 0 {
            print ("success!")
            
            GooglePlaceAPI.textSearch(query: inputValue){ ( statusCode, json) in
                if let jsonObj = json{
                    let places = APIParser.parseAPIResponse(json: jsonObj)
                    
                    DispatchQueue.main.async {
                        let resultsViewController = self.storyboard?.instantiateViewController(withIdentifier: "SearchResultsViewController") as! SearchResultsViewController
                        resultsViewController.places = places
                        self.navigationController?.pushViewController(resultsViewController, animated: true)
                        
                        
                    }
                    print("\(places.count)")
                }else {
                    self.generalAlert(title: "Opps", message: "An Error in parsing json")
                }
            }
            
            
//            generalAlert(title:"success!" , message: "we got \(inputValue)")
        }else{
            alertError()
        }
        
    }
    
    func alertError(){
        let alertController = UIAlertController(title: "Opps", message: "An Error...", preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
            self.dismiss(animated: true, completion: nil)
        }
        alertController.addAction(okAction)
        
        present(alertController, animated: true) {
            self.searchParameterTextField.placeholder = "Input something"
        }
    }
    
    @IBOutlet weak var segmentedControll: UISegmentedControl!
    
    @IBAction func searchSelectionChanged(_ sender: UISegmentedControl) {
        
        print("segment control changed \(segmentedControll.selectedSegmentIndex)")
        
        
        
    }
    @IBOutlet weak var searchSelctionChanged: UISegmentedControl!
    
        func generalAlert(title: String, message: String){
            let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
            
            let okAction = UIAlertAction(title: "Ok", style: .default) { (action) in
                self.dismiss(animated: true, completion: nil)
            }
            alertController.addAction(okAction)
            
            present(alertController, animated: true) {
                self.searchParameterTextField.placeholder = "Input something"
            }
    }

    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    @IBAction func filterAction(_ sender: Any) {
        let filterViewController = self.storyboard?.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        self.present(filterViewController, animated: true, completion: nil)
    }
    
    
}
// MARK: - Text Field Delegate
extension SearchViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchParameterTextField{
            textField.resignFirstResponder()
            return true
        }
        return false
    }
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool{
        if textField == searchParameterTextField{
            searchParam = textField.text
            print(textField.text ?? "No Value")
        }
        return true
    }
}

