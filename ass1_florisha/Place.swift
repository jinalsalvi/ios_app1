//
//  PlaceOfInterest.swift
//  ass1_florisha
//
//  Created by Jinal on 2018-06-18.
//  Copyright © 2018 Jinal. All rights reserved.
//

import Foundation

class PlaceOfInterest {
 
    var id: String
    var name: String
    var rating: Double?
    var formattedAddress: String?
    
}
