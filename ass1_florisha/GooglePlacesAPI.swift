//
//  GooglePlacesAPI.swift
//  ass1_florisha
//
//  Created by Jinal on 2018-06-18.
//  Copyright © 2018 Jinal. All rights reserved.
//

import Foundation

class GooglePlaceAPI{
    class func textSearch(query: String, completion: @escaping (_ statusCode: Int, _ json: [String: Any]?) -> Void){
        var urlComponents = URLComponents()
        urlComponents.scheme = Constants.scheme
        urlComponents.host = Constants.host
        urlComponents.path = Constants.testPlaceSearch
        
        urlComponents.queryItems  = [
            URLQueryItem(name:"query", value: query),
            
            URLQueryItem(name:"key", value: Constants.apiKey),
        ]
        
        NetworkingLayer.getRequest(with: urlComponents) { ( statusCode, data) in
            if let jsonData = data, let jsonObject = try?
            JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String: Any]{
                
                print(jsonObject ?? "")
                completion(statusCode,jsonObject)
                
                
            }else{
                print("Nothing found")
                completion(statusCode,nil)
            }
            
        }
        
    }
}
