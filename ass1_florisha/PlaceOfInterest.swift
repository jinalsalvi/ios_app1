//
//  PlaceOfInterest.swift
//  ass1_florisha
//
//  Created by Jinal on 2018-06-18.
//  Copyright © 2018 Jinal. All rights reserved.
//

import Foundation

class PlaceOfInterest {
    
    var id: String
    var name: String
    var rating: Double?
    var formattedAddress: String?
    var icon: String?
    var photo_reference: String?
    var lat: Double?
    var lng: Double?
    
    init? (json:[String: Any]){
        
        
        
        guard let id = json["id"] as? String  else {
            return nil
        }
        
        guard let name = json["name"] as? String  else {
            return nil
        }
        
        self.id = id
        self.name = name
        
        self.rating = json["rating"] as? Double
        
        self.formattedAddress = json["formatted_address"] as? String
        
        let image = json["photos"] as? [[String: Any]]
        self.photo_reference = image![0]["photo_reference"] as? String
        self.icon = json["icon"] as? String
        
        let geometry = json["geometry"] as? [String:AnyObject]
        let location = geometry!["location"] as? [String:Double]
        self.lat = location!["lat"]
        self.lng = location!["lng"]
    }
}
