
import UIKit
import MapKit

class DetailsViewController: UIViewController {

    var place: PlaceOfInterest?
    @IBOutlet weak var mapVIew: MKMapView!
    
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if let value = place?.photo_reference {
            imageView.downloadedFrom(url: Constants.getUrl(photoReference: value))
            }
        addressLabel.text = place?.formattedAddress
        let initialLocation = CLLocation(latitude: (place?.lat)!, longitude: (place?.lng)!)
        centerMapOnLocation(location: initialLocation, name: (place?.name)!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation, name: String) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
                                                                  regionRadius, regionRadius)
        mapVIew.setRegion(coordinateRegion, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        annotation.title = name
        mapVIew.addAnnotation(annotation)
        
    }

}
